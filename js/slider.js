"use strict";

window.addEventListener("load", function () {
    let slider = new Slider('.slider');
    console.log(slider);
});

class Slider {
    constructor(sl) {
        this.sl = document.querySelector(sl);

        this.imgs = this.sl.getElementsByTagName('img');
        this.show = document.createElement('div');
        this.prevBtn = document.createElement('div');
        this.nextBtn = document.createElement('div');
        this.dots = document.createElement('div');

        this._i = 0;
        this._disable = false;
        this._v = 15; // время переключения слайдера (20 * this._v) мс
        this.dotsArr = [];
        this.dotsTrueArr = [];
        this.width = parseInt(getComputedStyle(this.sl).width, 10);
        this.height = parseInt(getComputedStyle(this.sl).height, 10);
        this.fSize = this.width / 10;

        // стили генерируемых элементов
        this._btnStyle = [
            'color: rgba(255, 255, 255, 0.5)',
            'text-align: center',
            `font-size: ${this.fSize}px`,
            `line-height: ${this.height}px`,
            'position: absolute',
            'width: 15%',
            'height: 100%',
            'top: 0',
            'cursor: pointer',
            'transition: 0.3s'
        ];
        this._btnHoverStyle = [
            'color: rgba(255, 255, 255, 0.9)'
        ];
        this._dotsDivStyle = [
            'position: relative',
            'top: 90%',
            'display: flex',
            'justify-content: center',
            'align-items: center'
        ];
        this._dotStyle = [
            'width: 10px',
            'height: 10px',
            'margin: 0 10px',
            'border: 2px solid rgba(255, 255, 255, 0.8)',
            'border-radius: 50%',
            'cursor: pointer',
            'background-color: rgba(0, 0, 0, 0.2)'
        ];
        this._dotActiveStyle = [
            'background-color: rgba(255, 255, 255, 0.8)'
        ];

        // отлов ховер-эффекта на кнопки
        this.prevBtn.addEventListener('mouseover', ()=>{
            this.prevBtn.style.cssText += this._btnHoverStyle.join(';\ ');
        });
        this.prevBtn.addEventListener('mouseout', ()=>{
            this.prevBtn.style.cssText = this._btnStyle.join(';\ ');
            this.prevBtn.style.cssText += 'left: 0;';
        });
        this.nextBtn.addEventListener('mouseover', ()=>{
            this.nextBtn.style.cssText += this._btnHoverStyle.join(';\ ');
        });
        this.nextBtn.addEventListener('mouseout', ()=>{
            this.nextBtn.style.cssText = this._btnStyle.join(';\ ');
            this.nextBtn.style.cssText += 'right: 0;';
        });

        //удаляем выделение кнопки при многократном нажатии на неё
        this.prevBtn.onmousedown = this.prevBtn.onselectstart = function () {
            return false;
        };
        this.nextBtn.onmousedown = this.nextBtn.onselectstart = function () {
            return false;
        };

        // разворачиваем слайдер
        this.sliderReady();

        // подвешиваем события
        this.prevBtn.addEventListener('click', this.prev.bind(this));
        this.nextBtn.addEventListener('click', this.next.bind(this));
        for (let i = 0; i < this.dotsArr.length; i++){
            this.dotsArr[i].addEventListener('click', this.dotsMove.bind(this));
        }
    }
    // КОНЕЦ КОНСТРУКТОРА

    // МЕТОДЫ

    // создание кнопки Назад
    createBtnPrev() {
        this.prevBtn.className = 'prev';
        this.sl.appendChild(this.prevBtn);
        this.prevBtn.innerHTML = '&#x25c4';
        this.prevBtn.style.cssText = this._btnStyle.join(';\ ');
        this.prevBtn.style.cssText += 'left: 0;';
    }

    // создание кнопки Вперед
    createBtnNext() {
        this.nextBtn.className = 'next';
        this.sl.appendChild(this.nextBtn);
        this.nextBtn.innerHTML = '&#x25ba';
        this.nextBtn.style.cssText = this._btnStyle.join(';\ ');
        this.nextBtn.style.cssText += 'right: 0;';
    }

    //создание dots
    createDots() {
        this.dots.className = 'dots';
        this.sl.appendChild(this.dots);
        this.dots.style.cssText = this._dotsDivStyle.join(';\ ');
        let dotItem;
        for (let i = 0; i < this.imgs.length; i++){
            dotItem = document.createElement('div');
            dotItem.className = 'dot dot'+i;
            this.dots.appendChild(dotItem);
            dotItem.style.cssText = this._dotStyle.join(';\ ');
        }
        this.dotsArr = this.sl.querySelectorAll('.dot');
        this.dotsArr[this._i].classList.add('active');
        this.dotsArr[this._i].style.cssText += this._dotActiveStyle.join(';\ ');
        this.dotsTrueArr = Array.prototype.slice.call(this.dotsArr); // преобразую NodeList в Array, чтобы отлавливать номер элемента dot, на котором произошло событие
    }

    // создание обертки для слайдера
    createShow() {
        this.show.className = 'show';
        this.sl.insertBefore(this.show, this.sl.querySelector('img'));
        for (let i of this.imgs) {
            this.show.insertBefore(i, null);
        }
        this.show.style.cssText = 'display: flex; \ ' +
            'position: absolute;';
        this.show.style.left = -(this._i * this.width) + 'px'

    }

    //разворачиваем слайдер
    sliderReady() {
        this.sl.style.overflow = 'hidden';
        this.createBtnPrev();
        this.createBtnNext();
        this.createDots();
        this.createShow();
    }

    //переключение слайдера назад
    prev(e, startSl = this._i, nextSl = this._i - 1, callback) {
        if (this._disable === false) {

            if(nextSl < 0){
                this.show.style.left = - (this.width) + 'px';
                this.prevFirst(e);
                return;
            }

            this._disable = true;
            let pos = startSl * this.width;
            let t = setInterval(() => {
                if (pos <= nextSl * this.width + 1) {
                    this.dotActiveChange(e, startSl, nextSl);
                    this._i = nextSl;
                    if (callback){
                        callback();
                        this.dotActiveChange(e, 0, this._i);
                    }
                    clearInterval(t);
                    this._disable = false;
                    return;
                }
                pos -= (startSl - nextSl) * this.width / this._v;
                this.show.style.left = -(pos) + 'px';
            }, 20,);
        }
    }

    // назад из крайнего левого положения слайдера
    prevFirst(e){
        let temp = this.imgs[this.imgs.length - 1].cloneNode(true);
        this.show.insertBefore(temp, this.show.firstChild);
        this.prev(e, 1, 0, ()=> {
            this.show.removeChild(temp);
            this.show.style.left = - (this.imgs.length - 1) * this.width + 'px';
            this._i = this.imgs.length - 1;
        });
    }

    // переключение слайдера вперед
    next(e, startSl = this._i, nextSl = this._i + 1, callback) {
        if (this._disable === false) {

            if(nextSl >= this.imgs.length){
                this.nextLast(e, startSl, nextSl);
                return;
            }

            this._disable = true;
            let pos = startSl * this.width;
            let t = setInterval(() => {
                if (pos >= nextSl * this.width - 1) {
                    this.dotActiveChange(e, startSl, nextSl);
                    this._i = nextSl;
                    if (callback){
                        callback();
                    }
                    clearInterval(t);
                    this._disable = false;
                    return;
                }
                pos += (nextSl - startSl) * this.width / this._v;
                this.show.style.left = -(pos) + 'px';
            }, 20);
        }

    }

    // вперед из крайнего правого положения
    nextLast(e, startSl, nextSl){
        let temp = this.imgs[0].cloneNode(true);
        this.show.appendChild(temp);
        this.next(e, startSl, nextSl, ()=> {
            this.show.removeChild(temp);
            this.show.style.left = 0;
            this._i = 0;
        });
    }

    //переключение по dots
    dotsMove(e) {
        let startSl = this._i;
        let nextSl = this.dotsTrueArr.indexOf(e.target);
        if(startSl > nextSl){
            this.prev(e, startSl, nextSl)
        }else{
            this.next(e, startSl, nextSl)
        }
    }

    // смена отображения активного dot
    dotActiveChange(e, startSl, nextSl){
        if(startSl >= this.dotsArr.length){ // условие для слайдера с одним изображением
            startSl = 0;
        }
        this.dotsArr[startSl].classList.remove('active');
        this.dotsArr[startSl].style.cssText = this._dotStyle.join(';\ ');
        if(nextSl >= this.dotsArr.length){
            nextSl = 0;
        }
        this.dotsArr[nextSl].classList.add('active');
        this.dotsArr[nextSl].style.cssText += this._dotActiveStyle.join(';\ ');
    }
}